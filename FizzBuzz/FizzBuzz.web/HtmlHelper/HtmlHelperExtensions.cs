﻿namespace FizzBuzz.Web
{
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzz.Web.ViewModels;

    public static class HtmlHelperExtensions
    {
        public static HtmlString RenderView(this HtmlHelper helper, FizzBuzzViewModel fizzBuzzViewModel)
        {
            string htmlCode = "<ul>";
            foreach (string result in fizzBuzzViewModel.ResultList)
            {
               // htmlCode = htmlCode + "<li>";
                string splitText = " ";
                foreach (string word in result.Split(' '))
                {
                    if (result.Split(' ').Length > 1)
                    {
                        if (splitText != " ")
                        {
                            htmlCode = htmlCode + "<p class='" + @word + "'> " + @word + "</p></li>";
                            splitText = " ";
                        }
                        else
                        {
                            splitText = "<li> <p class='" + @word + "'>" + @word + "</p>";
                            htmlCode = htmlCode + splitText;
                        }
                    }
                    else
                    {
                        splitText = "<li> <p class='" + @word + "'>" + @word + "</p>";
                        htmlCode = htmlCode + splitText + "</li>";
                        splitText = " ";
                    }
                }
            }

            htmlCode = htmlCode + "</ul>";
            return new HtmlString(htmlCode);
        }
    }
}