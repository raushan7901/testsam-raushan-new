﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    public interface IDivisibilityRule
    {
        bool IsDivisible(int number);

        string GetMessage();
    }
}
