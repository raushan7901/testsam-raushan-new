﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class DivisibleByFiveRule : IDivisibilityRule
    {
        private readonly IDayCheckerRule dayChecker;

        public DivisibleByFiveRule(IDayCheckerRule dayChecker)
        {
            this.dayChecker = dayChecker;
        }

        public bool IsDivisible(int value)
        {
            return value % 5 == 0;
        }

        public string GetMessage()
        {
            return dayChecker.IsSpecifiedDay(DateTime.Now.DayOfWeek) ? "WUZZ" : "BUZZ";
        }
    }
}