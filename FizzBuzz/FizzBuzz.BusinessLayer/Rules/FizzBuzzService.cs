﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<IDivisibilityRule> divisibilityRules;

        public FizzBuzzService(IEnumerable<IDivisibilityRule> divisibilityRules)
        {
            this.divisibilityRules = divisibilityRules;
        }

        public IEnumerable<string> GetMessages(int number)
        {
            var messages = new List<string>();
            for (int index = 1; index <= number; index++)
            {
                var isDivisible = this.divisibilityRules.Where(m => m.IsDivisible(index)).ToList();
                messages.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.GetMessage())) : index.ToString());
            }

            return messages;
        }
    }
}