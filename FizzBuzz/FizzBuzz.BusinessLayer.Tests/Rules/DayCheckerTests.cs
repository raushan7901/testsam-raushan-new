﻿namespace FizzBuzz.BusinessLayer.Tests.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class DayCheckerTests
    {
        private DayCheckerRule dayChecker;

        [SetUp]
        public void SetUpTest()
        {
            this.dayChecker = new DayCheckerRule(DayOfWeek.Wednesday);
        }

        [Test]
        public void DayChecker_ReturnsTrue_WhenDayIsWednesday()
        {
            // Act
            var actualOutput = dayChecker.IsSpecifiedDay(DayOfWeek.Wednesday);

            // Assert
            actualOutput.Should().Be(true);
        }

        [Test]
        public void DayChecker_ReturnsFalse_WhenDayIsNotWednesday()
        {
            // Act
            var actualOutput = dayChecker.IsSpecifiedDay(DayOfWeek.Monday);

            // Assert
            actualOutput.Should().Be(false);
        }
    }
}
