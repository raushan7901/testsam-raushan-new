﻿namespace FizzBuzz.BusinessLayer.Tests.FizzBuzzLogic
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByFiveTests
    {
        private Mock<IDayCheckerRule> mockDayChecker;
        private DivisibleByFiveRule divideByFive;

        [SetUp]
        public void TestSetup()
        {
            this.mockDayChecker = new Mock<IDayCheckerRule>();
            this.divideByFive = new DivisibleByFiveRule(mockDayChecker.Object);
        }

        [Test]
        public void DivisibilityByFive_ShouldReturnTrue_WhenNumberProvidedIsDivisibleByFive()
        {
            // Act
            var result = divideByFive.IsDivisible(5);

            // Assert
            result.Should().Be(true);
        }

        [Test]
        public void DivisibilityByFive_ShouldReturnFalse_WhenNumberProvidedIsNotDivisibleByFive()
        {
            // Act
            var result = divideByFive.IsDivisible(7);

            // Assert
            result.Should().Be(false);
        }

        [Test]
        public void GetMessage_ShouldReturnWuzz_WhenDayIsWednesday()
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(true);

            // Act
            var result = divideByFive.GetMessage();

            // Assert
            result.Should().Be("WUZZ");
        }

        [Test]
        public void GetMessage_ShouldReturnBUZZ_WhenDayIsNotWednesday()
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(false);

            // Act
            var result = divideByFive.GetMessage();

            // Assert
            result.Should().Be("BUZZ");
        }
    }
}
