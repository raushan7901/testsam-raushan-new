﻿namespace FizzBuzz.BusinessLayer.Tests.FizzBuzzLogic
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByThreeTests
    {
        private DivisibleByThreeRule divideByThree;
        private Mock<IDayCheckerRule> mockDayChecker;

        [SetUp]
        public void SetUpTest()
        {
            this.mockDayChecker = new Mock<IDayCheckerRule>();
            this.divideByThree = new DivisibleByThreeRule(mockDayChecker.Object);
        }

        [Test]
        public void DivisibilityByThree_ShouldReturnTrue__WhenNumberProvidedIsDivisibleByThree()
        {
            // Act
            bool actualOutput = divideByThree.IsDivisible(3);

            // Assert
            actualOutput.Should().Be(true);
        }

        [Test]
        public void DivisibilityByThree_ShouldReturnFalse__WhenNumberProvidedIsNotDivisibleByThree()
        {
            // Act
            bool actualOutput = divideByThree.IsDivisible(7);

            // Assert
            actualOutput.Should().Be(false);
        }

        [Test]
        public void GetMessage_ShouldReturnWizz_WhenDayIsWednesday()
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(true);

            // Act
            var result = divideByThree.GetMessage();

            // Assert
            result.Should().Be("WIZZ");
        }

        [Test]
        public void GetMessage_ShouldReturnFizz_WhenDayIsNotWednesday()
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(false);

            // Act
            var result = divideByThree.GetMessage();

            // Assert
            result.Should().Be("FIZZ");
        }
    }
}
